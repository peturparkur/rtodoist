use std::hash::Hash;

use serde::{Serialize, Deserialize, Deserializer, de};
use chrono::{self, TimeZone, DateTime, NaiveDateTime, Utc, Offset};
use uuid::Uuid;
// pub trait DeseSerialize<'de>: Serialize + Deserialize<'de> {}

// Individual task representation
// The connection between the tasks (dependencies, blockers) will be representated as edges within a DAG network!
#[derive(Serialize, Deserialize, Debug, Hash, Clone, Copy)]
pub struct Task<D, I>
where
    D: Clone,
    I: Clone + Hash + From<String>
{
    pub id: I,
    pub start_date: Option<chrono::DateTime<Utc>>, // start-date
    pub due_date: Option<chrono::DateTime<Utc>>, // when it should finish
    pub duration: Option<u128>, // amount of time it requires
    pub work: u128, // amount of work that was put into it
    pub description: D // implicitly depends on Serialize + Deserialize
}

impl<D, I> Task<D, I>
where
    D: Clone,
    I: Clone + Hash + From<String>
{
    pub fn new(description: D) -> Task<D, I> {
        return Task {
            id: I::from(Uuid::new_v4().simple().to_string()),
            start_date: Some(chrono::offset::Utc::now()),
            due_date: None,
            duration: None,
            work: 0,
            description
        };
    }
}

impl<D, I> From<TaskDto<D>> for Task<D, I>
where
    D: Clone,
    I: Clone + Hash + From<String>
{
    fn from(dto: TaskDto<D>) -> Self {
        return Self { 
            id: I::from(Uuid::new_v4().as_simple().to_string()), 
            start_date: dto.start_date, 
            due_date: dto.due_date, 
            duration: dto.duration, 
            work: 0, 
            description: dto.description 
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Hash, Clone, Copy)]
pub struct TaskDto<D>
where
    D: Clone
{
    pub description: D,
    pub start_date: Option<chrono::DateTime<Utc>>, // start-date
    pub due_date: Option<chrono::DateTime<Utc>>, // when it should finish
    pub duration: Option<u128>, // amount of time it requires
}

// impl<D> Into<Task<D>> for TaskDto<D> {
//     fn into(self) -> Task<D> {
//         Task {
//             id: 0u128, // todo
//             start_date: self.start_date,
//             due_date: self.due_date,
//             duration: self.duration,
//             work: 0,
//             description: self.desctiption,
//         }
//     }
// }