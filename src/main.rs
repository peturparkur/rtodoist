use std::{sync::Arc, collections::HashMap};

use actix_web::{HttpServer, App, Responder, web::{self, Data}, get, post, HttpRequest, HttpResponse, http::header::ContentType, delete};
use daggy::{Dag, NodeIndex};
use petgraph::{visit::{NodeIndexable, IntoNodeIdentifiers}, data::DataMap};
use serde_json::json;
use tokio::sync::{Mutex, RwLock};
mod structures;
// use structures::{task::Task};

use crate::structures::task::{Task, TaskDto};

#[get("/hello/{name}")]
async fn greet(name: web::Path<String>) -> impl Responder {
    format!("Hello {}!", name)
}

#[post("/tasks")]
async fn create_new_task(req: HttpRequest, item: web::Json<TaskDto<String>>, state: Data<AppState>) -> impl Responder {

    let task = Task::<String, String>::from(item.into_inner());
    let task_id = task.id.clone();
    let id = state.graph
        .lock()
        .await
        .add_node(task.clone());

    state
        .id_map
        .write()
        .await
        .insert(task_id, id);

    HttpResponse::Ok()
    .content_type(ContentType::json())
    .json(task)
}

#[get("/tasks/active")]
async fn get_active_tasks(state: Data<AppState>) -> impl Responder {
    let tasks = state
        .graph
        .lock()
        .await
        .node_weights_mut()
        .into_iter()
        .filter(|x| x.start_date.map_or(false, |date| date < chrono::offset::Utc::now()))
        .filter(|x| x.due_date.map_or(true, |date| date > chrono::offset::Utc::now()))
        .map(|x| x.clone())
        .collect::<Vec<_>>();
    
    HttpResponse::Ok()
        .content_type(ContentType::json())
        .json(tasks)
}

#[get("/tasks/{id}")]
async fn get_task(req: HttpRequest, path: web::Path<String>, state: Data<AppState>) -> impl Responder {
    let id = path.into_inner();
    let node_id = match state
        .id_map
        .read()
        .await
        .get(&id) {
            Some(x) => x.clone(),
            None => return HttpResponse::BadRequest()
            .content_type(ContentType::json())
            .json(json!({"Code": "Task index doesn't exist"}))
        };
    
    let task = match state
        .graph
        .lock()
        .await
        .node_weight(node_id) {
            Some(x) => x.clone(),
            None => return HttpResponse::BadRequest()
            .content_type(ContentType::json())
            .json(json!({"Code": "Task couldn't be found"}))
        };

    HttpResponse::Ok()
        .content_type(ContentType::json())
        .json(task)
}

#[delete("/tasks/{id}")]
async fn delete_task(req: HttpRequest, path: web::Path<String>, state: Data<AppState>) -> impl Responder {
    let id = path.into_inner();
    let node_id = match state
        .id_map
        .read()
        .await
        .get(&id) {
            Some(x) => x.clone(),
            None => return HttpResponse::BadRequest()
            .content_type(ContentType::json())
            .json(json!({"Code": "Task index doesn't exist"}))
        };
    
    let task = match state
        .graph
        .lock()
        .await
        .remove_node(node_id) {
            Some(x) => x,
            None => return HttpResponse::BadRequest()
            .content_type(ContentType::json())
            .json(json!({"Code": "Task couldn't be found"}))
        };
    
    HttpResponse::Ok()
        .content_type(ContentType::json())
        .json(task)
    
}


#[derive(Clone, Debug)]
struct AppState {
    graph: Arc<Mutex<Dag<Task<String, String>, f64, usize>>>,
    id_map: Arc<RwLock<HashMap<String, NodeIndex<usize>>>>
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    println!("Hello, world!");

    let state = AppState {
        graph: Arc::new(Mutex::new(daggy::Dag::<Task<String, String>, f64, usize>::new())),
        id_map: Arc::new(RwLock::new(HashMap::new()))
    };

    HttpServer::new(move || {
        App::new()
            .service(greet)
            .service(create_new_task)
            .service(get_task)
            .service(delete_task)
            .service(get_active_tasks)
            .app_data(web::Data::new(state.clone()))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
